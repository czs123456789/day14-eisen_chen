package com.chen.todoapi;

import com.chen.todoapi.entity.Todo;
import com.chen.todoapi.repository.JPATodoRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/27/2023
 **/

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @BeforeEach
    void setUp() {
        jpaTodoRepository.deleteAll();
    }

    @Test
    void should_find_all_when__given_() throws Exception {
        Todo todo = getTodoRequest();
        jpaTodoRepository.save(todo);

    //given
        List<Todo> all = jpaTodoRepository.findAll();
        //when
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(all.size()));
     //then
    }

    @Test
    void should_create_todo() throws Exception {
        //given
        Todo todoRequest = getTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);
        //when
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.getDone()));
        //then
    }
    
    @Test
    void should_delete_todo_item_by_id() throws Exception {
    //given
        Todo todo = getTodoRequest();
        jpaTodoRepository.save(todo);
     //when
        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(jpaTodoRepository.findById(todo.getId()).isEmpty());
     //then
    }

    @Test
    void should_update_todo() throws Exception {
        Todo previousTodo = new Todo(null, "aaaa", true);
        jpaTodoRepository.save(previousTodo);

        Todo todoUpdateRequest = new Todo(null, "bbbbb", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoUpdateRequest);
        mockMvc.perform(put("/todos/{id}", previousTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Todo> optionalTodo = jpaTodoRepository.findById(previousTodo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(todoUpdateRequest.getText(), updatedTodo.getText());
        Assertions.assertEquals(todoUpdateRequest.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_given_exception_when_get_given_is_not_exit() throws Exception {
        Todo todoUpdateRequest = new Todo(null, "bbbbb", false);
        Todo save = jpaTodoRepository.save(todoUpdateRequest);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoUpdateRequest);
    //given
        mockMvc.perform(put("/todos/{id}", save.getId()+1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo id not found"));
        //when

     //then
    }


    private Todo getTodoRequest() {
        Todo todo = new Todo();
        todo.setDone(true);
        todo.setText("风吹吹而飘衣");
        return todo;
    }
}
