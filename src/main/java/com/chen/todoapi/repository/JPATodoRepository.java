package com.chen.todoapi.repository;

import com.chen.todoapi.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/27/2023
 **/

@Repository
public interface JPATodoRepository extends JpaRepository<Todo, Long> {

}
