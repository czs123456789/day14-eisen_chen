package com.chen.todoapi.service;

import com.chen.todoapi.entity.Todo;
import com.chen.todoapi.exception.TodoNoFoundException;
import com.chen.todoapi.repository.JPATodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/27/2023
 **/

@Service
public class TodoService {

    private final JPATodoRepository jpaTodoRepository;

    public TodoService(JPATodoRepository jpaTodoRepository) {
        this.jpaTodoRepository = jpaTodoRepository;
    }

    public List<Todo> findAll() {
        return jpaTodoRepository.findAll();
    }

    public void deleteById(Long id) {
        jpaTodoRepository.deleteById(id);
    }

    public Todo save(Todo todo) {
        return jpaTodoRepository.save(todo);
    }

    public Todo findById(Long id) {
        return jpaTodoRepository.findById(id).orElseThrow(TodoNoFoundException::new);
    }

    public void update(Long id, Todo todo) {
        Todo currTodo = findById(id);
        if (todo.getDone() != null) {
            currTodo.setDone(todo.getDone());
        }
        if (todo.getText() != null) {
            currTodo.setText(todo.getText());
        }
        jpaTodoRepository.save(currTodo);
    }
}
