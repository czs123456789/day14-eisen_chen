package com.chen.todoapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/27/2023
 **/
@ResponseStatus(HttpStatus.NOT_FOUND)
public class TodoNoFoundException extends RuntimeException {
    public TodoNoFoundException() {
        super("todo id not found");
    }
}
