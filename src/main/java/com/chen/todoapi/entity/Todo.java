package com.chen.todoapi.entity;

import javax.persistence.*;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/27/2023
 **/
@Entity
@Table(name = "todoList")
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private Boolean done;

    public Todo() {
    }

    public Todo(Long id, String text, Boolean done) {
        this.id = id;
        this.text = text;
        this.done = done;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
