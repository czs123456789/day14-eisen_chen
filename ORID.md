Objective：
1、Code View & Integration with back-end.

2、Draw concept map with frontend.

3、Listen to a lecture on agile development knowledge.

Reflective: Gain a lot of knowledge.

Interpretive：
Continue to learn knowledge related to front-end.

Decisional：
I have learned about cross domain knowledge. At the same time, seeing the beautiful front-end interface of others, I will learn from them.
